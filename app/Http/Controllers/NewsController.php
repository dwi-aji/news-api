<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Call News Model
use App\News;

class NewsController extends Controller
{
    /**
     * Get All Available Sources
     */
    public function showSources(){
        $api = new News;

        return $api->allSources();
    }

    /**
     * Get All News based on Selected Source
     * 
     */
    public function showNews(Request $request){
        $api = new News;

        return $api->getTopNews($request->sources);
    }

    /**
     * Get All News based on Search Keyword
     * 
     */
    public function showSearch(Request $request){
        $api = new News;

        return $api->getSearchNews($request->keyword);
    }

    /**
     * Display Index Page
     * 
     */
    public function index(){
        $sources = $this->showSources();

        return view('index', compact('sources'));
    }

    /**
     * Display Search Page
     * 
     */
    public function search(){
        return view('search');
    }
}
