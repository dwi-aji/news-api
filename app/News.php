<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

// Call HTTPClient to make API Request
use GuzzleHttp\Client;

class News extends Model
{
    /**
     * Print all sources
     * 
     * HTTP Request made by Guzzle
     */
    public function allSources(){
        $client = new Client;
        
        $sources = $client->request('GET', 'https://newsapi.org/v2/sources?language=en&apiKey=ac5eefffb7dc4690914066b6bf2d6beb');
        $sources_content = json_decode($sources->getBody(), true);
        return $sources_content['sources'];
    }

    /**
     * Print All News based on Selected Source
     * 
     */
    public function getTopNews($source){
        $client = new Client;

        $news = $client->request('GET', 'https://newsapi.org/v2/top-headlines?sources='.$source.'&apiKey=ac5eefffb7dc4690914066b6bf2d6beb');
        $news_content = json_decode($news->getBody(), true);
        return $news_content['articles'];
    }

    /**
     * Print All News based on Search Keyword
     * 
     */
    public function getSearchNews($keyword){
        $client = new Client;

        $news = $client->request('GET', 'https://newsapi.org/v2/everything?q='.$keyword.'&apiKey=ac5eefffb7dc4690914066b6bf2d6beb');
        $news_content = json_decode($news->getBody(), true);
        return $news_content['articles'];
    }
}
