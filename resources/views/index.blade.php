<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Bootstrap CSS -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="wrapper text-center">
                <h2 class="mt-3">NewsAPI</h2>

                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/') }}">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/search') }}">Search</a>
                    </li>
                </ul>

                <hr>

                <form id="sourcesForm">{{-- Form --}}
                    <label for="sources">Select Source</label>
                    <select class="form-control" name="sources" id="sources_id">
                        @foreach ($sources as $item)
                            <option value="{{ $item['id'] }}">{{ $item['name'] }}</option>
                        @endforeach
                    </select>
                </form>{{-- Form --}}

                <h5 id="selected_source" class="text-left mt-1"></h5>
                <div id="newsSection" class="row mt-3"></div>
            </div>
        </div>
    </body>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>

    <!-- Inline Javascript -->
    <script>
        $(document).ready(function(){
            refreshFeed();
        });
        $.ajaxSetup({ {{-- Set csrf token for every ajax request --}}
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        });

        $("#sources_id").change(function(){
            // console.log("Source changed, current selected source : "+$("#sources_id").val());
            $("#newsSection").empty();
            $("#selected_source").text("Selected source : -");
            refreshFeed();
        });

        function refreshFeed(){
            $.ajax({
                method: "POST",
                url: "{{ url('/news') }}",
                data: $("#sourcesForm").serialize(),
                cache: false,
                success: function(result){
                    $("#selected_source").text("Selected source : "+result['0']['source'].name);

                    $.each(result, function(index, value){
                        if(value['urlToImage'] == null){
                            var urlImage = "https://semantic-ui.com/images/wireframe/image.png";
                        } else {
                            var urlImage = value['urlToImage'];
                        }
                        $("#newsSection").append('<div class="col-12 col-lg-3 mt-2"><div class="card"><img class="card-img-top img-fluid" style="height:145px;" src="'+urlImage+'" alt="Card image cap"><div class="card-body"><h5 class="card-title text-left">'+value['title']+'</h5><hr class="my-0"><p class="text-left">'+value['description']+' <a href="'+value['url']+'">Read More</a></p></div></div></div>')
                    });
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log(jqXHR);
                }
            });
        }
    </script>
</html>
