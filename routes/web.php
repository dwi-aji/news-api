<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'NewsController@index');
Route::get('/search', 'NewsController@search');
Route::post('/search', 'NewsController@showSearch');
Route::get('/sources', 'NewsController@showSources');
Route::post('/news', 'NewsController@showNews');
